<img src="https://gitlab.com/victoreke/tasky/-/raw/main/src/banner.png" alt="Tasky Banner Image">
<br/>

<div>
<img src="https://gitlab.com/victoreke/tasky/-/raw/main/src/assets/logo.png" width="60" height="50">

<h1>Tasky</h1>
</div>

Tasky is a simple todo application for managing tasks. Built with Angular and SCSS.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.5.

## Development server

```js
ng serve
```

Run the above command for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Run local mock backend

Run the following command in your terminal

```js
// this
npm run server

// or
json-server db.json --watch --port 5000
```

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Code scaffolding

Run this code to generate a new component. You can alternatively prefix the component with a directory.

```js
ng generate component <component>`
```

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
