// Import component from core
import { Component } from '@angular/core';

// Declaration of the component
@Component({
  // Embed component using the selectors (All components will be embedded here)
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
// Add properties or methods
export class AppComponent {}
